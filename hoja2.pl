% Prolog is here

% Ejercicio 2
% arista(Propiedad, Origen, Destino). :- (Origen) --> Propiedad --> (Destino)

% Humanos
arista(instancia, juan, humano).
arista(instancia, mario, humano).
arista(instancia, pablo, humano).

% Vehiculo
arista(subclase, coche, vehiculo).
	arista(tiene, coche, sistema_electrico).
	arista(subclase, seat, coche).
		arista(instancia, c_0, seat).

% Bateria
arista(tiene, bateria, acido).
	arista(tiene, sistema_electrico, bateria).

% Producto Quimico
arista(subclase, acido, producto_quimico).	

% Prestamo
arista(subclase, prestamo, suceso).
	arista(instancia, pr_0, prestamo).
		arista(prestado, pr_0, maria).
		arista(prestador, pr_0, juan).
		arista(objeto, pr_0, c_0).
		arista(tiempo, pr_0, t_0).
	arista(instancia, pr_1, prestamo).
		arista(prestado, pr_1, pablo).
		arista(prestador, pr_1, maria).
		arista(objeto, pr_1, c_0).
		arista(tiempo, pr_1, t_1).

% Enfado
arista(subclase, enfado, suceso).
	arista(instancia, enf_0, enfado).
		arista(enfadado, enf_0, juan).
		arista(receptor, enf_0, maria).
		arista(tiempo, enf_0, ahora).

% Tiempo
arista(instancia, t_0, tiempo).
arista(instancia, t_1, tiempo).
arista(instancia, ahora, tiempo).

% Generales
arista_tr(Pr, Ori, Dest) :- arista(Pr, Ori, Dest).
arista_tr(Pr, Ori, Dest) :- arista(Pr, Ori, Z), arista_tr(Pr, Z, Dest).

arista_h(Pr, Ori, Dest) :- arista_tr(Pr, Ori, Dest).
arista_h(Pr, Ori, Dest) :- 
	hereda_de(Pr, New), 
	arista_h(Pr, Ori, X),
	arista_tr(New, X, Dest).
arista_h(Pr, Ori, Dest) :- 
	hereda_de(Pr, New),
	arista_h(Pr, X, Dest),
	arista_tr(New, Ori, X).

% Herencias
hereda_de(instancia, subclase).
hereda_de(tiene, subclase).

% arista_h(tiene, seat, sistema_electrico).
